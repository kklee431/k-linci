# You can install it with Termux

### Requirements
- python 3.9^
- ARM 64bit (aarch64)

### Clone repo and install module first
```sh
pkg update && pkg upgrade
pkg install python git
git clone https://gitlab.com/kklee431/k-linci.git
pip install requests bs4 futures
```

### Go to directory
```sh
cd k-linci
```

### Then run the scripts
```sh
python run.py
```

if you need license key, please contact on telegram [@BaruDaftarTelegram](https://t.me/BaruDaftarTelegram)
